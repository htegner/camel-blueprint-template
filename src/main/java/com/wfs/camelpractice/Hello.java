package com.wfs.camelpractice;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
