INTRO
=====

This project was generated from a maven archetype with the following command:
```
mvn archetype:generate \
  -DarchetypeGroupId=org.apache.camel.archetypes \
  -DarchetypeArtifactId=camel-archetype-blueprint \
  -DarchetypeVersion=2.22.0 
```

This will start a wizard that asks you for certain properties for your project.



Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.wfs.camelpractice/blueprinttemplate/1.0-SNAPSHOT

(In Karaf you don't need the osgi prefix.)

For more help see the Apache Camel documentation

    http://camel.apache.org/


